const express = require('express')
const bodyParser = require('body-parser')
const xss = require('xss-clean')
const cors = require('cors')
const helmet = require('helmet')
const { NotFoundError } = require('./errors')
const setupRoutes = require('./routes')
const errorHandler = require('./middlewares/error-handler')

const createApp = async () => {
  const app = express()
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(xss())
  app.use(helmet())
  app.use(cors({
    exposedHeaders: ['Content-Disposition']
  }))

  // Set global response headers
  app.use((req, res, next) => {
    res.setHeader('Content-Type', 'application/json')
    req.locals = {}
    next()
  })

  await setupRoutes(app)

  app.use(errorHandler())

  // Not Found 404
  app.use((req, res) => {
    res.status(404).json(new NotFoundError('Página não encontrada').toJSON())
  })

  return app
}

module.exports = createApp
