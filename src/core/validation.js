const Joi = require('joi')
const validate = require('express-joi-validation').createValidator({ passError: true })
const moment = require('moment')
const { ValidationError } = require('../errors')

const isDate = (value) => {
  if (!moment(value).isValid()) {
    throw new ValidationError('date is not valid')
  }

  return value
}

module.exports = {
  Joi,
  validate,
  isDate,
}
