
const restaurants = [
  { value: 1, 'name': 'Bistrô do Solar' },
  { value: 2, 'name': 'Cocina Paisano' },
  { value: 3, 'name': 'Atelier de Massas' },
  { value: 4, 'name': 'Rocks Bar e Restaurante' },
  { value: 5, 'name': 'Koh Pee Pee' },
  { value: 6, 'name': 'Restaurante Quintanilha' },
  { value: 7, 'name': 'Thêmis Restaurante' },
  { value: 8, 'name': 'Panorama Gastronômico' },
  { value: 9, 'name': 'Casa Guandu' },
]

module.exports = () => restaurants