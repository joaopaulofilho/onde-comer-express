const list = require('../list')
const Votes = require('../../votes/model')

describe('Filtering restaurants list', function () {
  it('Remove a restaurant when it is the winner', function () {
    const restaurantId = 7
    const restaurantName = 'Thêmis Restaurante'
    const date = '2020-09-08'

    const { restaurants: firstList } = list(date)
    expect(firstList.find(({ name }) => name === restaurantName)).toBeTruthy()

    const { votes } = Votes()
    for (let x = 1; x <= 9; x++) {
      votes.push({
        restaurantId,
        restaurantName,
        cpf: '11111111111',
        date,
      })
    }

    const { restaurants: newList } = list(date)
    expect(newList.find(({ name }) => name === restaurantName)).toBeFalsy()
  })
})
