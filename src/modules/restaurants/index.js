const { Joi, validate, isDate } = require('../../core/validation')
const List = require('./list')

module.exports = function (app) {
  validate.query(Joi.object({
    date: Joi.custom(isDate).required(),
  })),

  app.get('/restaurants',
    (req, res) => {
      const { dates, restaurants: list } = List(req.query.date)

      return res.status(200).json({
        dates,
        list,
      })
    },
  )
}
