const moment = require('moment')
const getRanking = require('../votes/ranking')
const { isClosed } = require('../votes/ranking')
const Restaurants = require('./model')

const list = date => {
  let restaurants = Restaurants()

  const dates = []
  const today = parseInt(moment().format('e'), 10)

  for (let day = today; day >= 0; day--) {
    const calcDate = moment(date).add(-day, 'days').format('YYYY-MM-DD')

    const closed = isClosed(calcDate)

    if (closed) {
      const ranking = getRanking(calcDate)
      const [[restaurantName]] = ranking

      restaurants = restaurants.filter(r => r.name !== restaurantName)

      dates.push({ date: calcDate, name: restaurantName })
    }
  }

  return { dates, restaurants }
}

module.exports = list
