const Users = require('./model')

module.exports = function (app) {
  app.get('/users',
    (req, res) => {
      return res.status(200).json(Users.find())
    },
  )
}
