const { get } = require('lodash')
const Votes = require('./model')
const Restaurants = require('../restaurants/model')

const add = ({ cpf, restaurant, date }) => {
  const { votes } = Votes()
  const found = Restaurants().find(r => r.value === restaurant)
  votes.push({
    restaurantId: restaurant,
    restaurantName: get(found, 'name', ''),
    cpf,
    date,
  })
}

module.exports = add