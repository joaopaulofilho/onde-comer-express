const add = require('../add')
const Votes = require('../model')

describe('Adding votes', function () {
  it('Can add votes for restaurants', function () {
    expect(Votes().votes).toHaveLength(10)

    add({
      cpf: '99999999999',
      restaurant: 9,
      date: '2020-10-01',
    })

    expect(Votes().votes).toHaveLength(11)
  })
})
