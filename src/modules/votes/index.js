const { pick } = require('lodash')
const { Joi, validate, isDate } = require('../../core/validation')
const auth = require('../../middlewares/auth')
const validateVotingDate = require('../../middlewares/validate-voting-date')
const Votes = require('./model')
const add = require('./add')
const calcRanking = require('./ranking')
const { isClosed } = require('./ranking')

module.exports = function (app) {
  // boards
  app.get('/votes/results',
    validate.query(Joi.object({
      date: Joi.custom(isDate).required(),
    })),

    (req, res) => {
      const closed = isClosed(req.query.date)
      const ranking = calcRanking(req.query.date)
      return res.status(200).json({
        closed,
        ranking,
      })
    }
  )

  // voters today DEBUG
  app.get('/votes',
    (req, res) => {
      return res.status(200).json(Votes())
    },
  )

  // add votes
  app.post('/votes/add',

    validate.body(Joi.object({
      cpf: Joi.string().min(11).max(11).required(),
      restaurant: Joi.number().min(1).max(9).required(),
      date: Joi.custom(isDate).required(),
    })),

    auth(),

    validateVotingDate(),

    (req, res) => {
      add(pick(req.body, ['cpf', 'restaurant', 'date']))
      return res.status(201).send()
    }
  )
}
