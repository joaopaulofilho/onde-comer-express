const Users = require('../users/model')
const Votes = require('../votes/model')

const users = Users()

const isClosed = date => {
  const { votes } = Votes()
  const pollOfTheDay = votes.filter(v => v.date === date)

  return users.length === pollOfTheDay.length
}

const pollResult = date => {
  const { votes } = Votes()
  const pollOfTheDay = votes.filter(v => v.date === date)

  const counting = {}

  pollOfTheDay.forEach(poll => {
    if (!counting[poll.restaurantName]) {
      counting[poll.restaurantName] = 0
    }

    counting[poll.restaurantName] += 1
  })

  const ranking = Object.entries(counting)
  ranking.sort((a, b) => parseInt(b[1], 10) - parseInt(a[1], 10))

  return ranking.slice(0, 3)
}

module.exports = pollResult
module.exports.isClosed = isClosed