
// { restaurantId: Number, voter: String, date: String }
const votes = [
  { restaurantId: 1, restaurantName: 'Bistrô do Solar', cpf: '11111111111', date: '2020-09-06' },
  { restaurantId: 1, restaurantName: 'Bistrô do Solar', cpf: '55555555555', date: '2020-09-06' },
  { restaurantId: 2, restaurantName: 'Cocina Paisano', cpf: '22222222222', date: '2020-09-06' },
  { restaurantId: 2, restaurantName: 'Cocina Paisano', cpf: '33333333333', date: '2020-09-06' },
  { restaurantId: 2, restaurantName: 'Cocina Paisano', cpf: '44444444444', date: '2020-09-06' },
  { restaurantId: 5, restaurantName: 'Koh Pee Pee', cpf: '66666666666', date: '2020-09-06' },
  { restaurantId: 5, restaurantName: 'Koh Pee Pee', cpf: '77777777777', date: '2020-09-06' },
  { restaurantId: 5, restaurantName: 'Koh Pee Pee', cpf: '88888888888', date: '2020-09-06' },
  { restaurantId: 5, restaurantName: 'Koh Pee Pee', cpf: '99999999999', date: '2020-09-06' },

  { restaurantId: 1, restaurantName: 'Bistrô do Solar', cpf: '11111111111', date: '2020-09-07' },
]

module.exports = () => ({
  votes,
})