const { NotFoundError } = require('../errors')
const Users = require('../modules/users/model')

const auth = () => (req, res, next) => {
  const { cpf } = req.body

  const user = Users().find(u => u.cpf === cpf)

  if (!user) {
    throw new NotFoundError('Invalid user')
  }

  next()
}

module.exports = auth