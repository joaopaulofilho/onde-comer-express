const { BadRequestError } = require('../errors')
const Votes = require('../modules/votes/model')

// eslint-disable-next-line
const validateDateVote = () => (req, res, next) => {
  const { cpf, date } = req.body

  const { votes } = Votes()

  const hasVoted = votes.find(v => v.cpf === cpf && v.date === date)

  if (hasVoted) {
    throw new BadRequestError('User already voted')
  }

  next()
}

module.exports = validateDateVote