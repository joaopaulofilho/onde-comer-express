const { get } = require('lodash')

const isJoiMiddlewareError = err => (
  get(err, 'value') !== undefined &&
  get(err, 'error._original') !== undefined
)

const debug = process.env.NODE_ENV === 'development'

module.exports = function errorHandler() {
  // eslint-disable-next-line
  return (err, req, res, next) => {
    const json = {}
    let status, details

    status = err.status || err.code || 500

    if (err.name === 'MongoError') {
      status = 400
      err.message = 'Duplication error'
      json.errors = { fields: Object.keys(err.keyPattern) }
    }

    if (isJoiMiddlewareError(err)) {
      status = 400
      err.message = 'One or more fields are invalid'
      json.errors = { fields: get(err, 'error.details').map(({ path: [fieldError]}) => fieldError) }
    }

    if (!json.errors && err.errors && Object.keys(err.errors).length > 0) {
      json.errors = err.errors
    }

    if (err.toString() !== '' && !err.message) {
      json.message = err.toString()
    }

    if (debug) {
      if (status === 400) {
        // eslint-disable-next-line
        console.log('VALIDATION ERROR', err)
      }

      if (status >= 500) {
        // eslint-disable-next-line
        console.error('ERROR HANDLER', err)
      }

      return res.status(status).send({
        message: err.message,
        errors: json.errors,
        details,
      })
    }

    res.status(status).send({
      message: status >= 500 ? 'Erro inesperado!' : err.message,
      errors: status >= 500 ? undefined : json.errors,
      details,
    })
  }
}
