const auth = require('../auth')

describe('Auth middleware', function () {
  const res = ({ status: () => ({ json: data => data }) })
  const next = jest.fn()

  it('Should validate user in body parameters', function () {
    const req = { body: { cpf: '11010101010' } }
    expect(() => auth()(req, res, next)).toThrow(/invalid user/i)

    req.body.cpf = '22222222222'
    auth()(req, res, next)
    expect(next).toHaveBeenCalledTimes(1)
  })
})