const validate = require('../validate-voting-date')

describe('Validate Voting Date middleware', function () {
  const res = ({ status: () => ({ json: data => data }) })
  const next = jest.fn()

  jest.mock('../../modules/votes/model', () => () => ({
    votes: [
      { restaurant: 1, voter: '11111111111', date: '2020-09-07' },
      { restaurant: 1, voter: '22222222222', date: '2020-09-08' },
    ],
  }))

  it('Should validate 1 vote per day per user', function () {
    const req = { body: {
      cpf: '11111111111',
      date: '2020-09-07',
    }}
    expect(() => validate()(req, res, next)).toThrow(/user already voted/i)

    req.body.cpf = '22222222222'
    validate()(req, res, next)
    expect(next).toHaveBeenCalledTimes(1)
  })
})
