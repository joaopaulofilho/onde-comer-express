# Onde Vamos Comer?
Aplicação para votação de restaurantes para almoçar com seus colegas e/ou amigos.

## Requisitos
- node v12+
- npm v6+
- docker v18+
- docker-compose v1.18+

## Iniciar o app
- docker-compose up
- Api servindo no endereço http://localhost:3030/

## Oportunidades para melhorias
- proteção de rotas por login
- melhor cobertura de testes automatizados
- utilização de banco de dados